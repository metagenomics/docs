# Metagenomics group documentation

## Documentations

Name | Description
---- | -----------
[Data simulation](simulation/) | Generate simulated metagenomics data for benchmarking
[Sunbeam](sunbeam/) | How to use sunbeam at Pasteur on TARS

## Projects and repository

Project | Description
------- | -----------
[workflow-benchmarking](https://gitlab.pasteur.fr/metagenomics/workflow-benchmarking) | Compare different workflow system (CWL, NextFlow and SnakeMake)

## Reading and Bibliography

A library has been created on [Zotero](https://www.zotero.org/groups/2312155/metagenomicspasteur/items). Feel free to add new
content and organize the library.
