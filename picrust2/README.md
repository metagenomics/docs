# PICRUSt2

[PICRUSt2](https://github.com/sunbeam-labs/sunbeam) is a software for predicting functional abundances based only
on marker gene sequences.

Here is a brief note to help you use PICRUSt2 at Pasteur on TARS. You can also read the
full [documentation](https://github.com/picrust/picrust2/wiki) or get more information from the 
[preprint](https://www.biorxiv.org/content/10.1101/672295v1).

## Install

### Prerequisite

[Conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) is required for the
basic install of the tool. We might set up a singularity image in the future.

### Procedure

Please refer to the [documentation](https://github.com/picrust/picrust2/wiki/Installation) to get the latest
procedure. You can [install from bioconda](https://github.com/picrust/picrust2/wiki/Installation#install-from-bioconda)
of [install from source](https://github.com/picrust/picrust2/wiki/Installation#install-from-source).

> **Warning** You need internet access to do that so you need to perform install on
the head of submission.

## Run PICRUSt2

You can follow the [documentation](https://github.com/picrust/picrust2/wiki/Full-pipeline-script)
to run the full picrust2 workflow.

### Tutorial

A tutorial is available [here](https://github.com/picrust/picrust2/wiki/PICRUSt2-Tutorial-(v2.2.0-beta)#tutorial-dataset)
