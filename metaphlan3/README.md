# Metaphlan3 and StrainPhlan3

## Install

> You need to perform install from the head of submission (tars-0) since an internet connection is needed

[Instructions here](https://github.com/biobakery/MetaPhlAn/wiki/MetaPhlAn-3.0#installation), in brief what has been done:

```bash
conda create --name mpa -c bioconda python=3.7 metaphlan
conda activate mpa  # or source activate mpa
metaphlan --install  # install db
```

## Run

You can try the following tutorials:

* [Metaphlan3](https://github.com/biobakery/MetaPhlAn/wiki/MetaPhlAn-3.0#basic-usage)
* [StrainPhlan3](https://github.com/biobakery/MetaPhlAn/wiki/StrainPhlAn-3.0#usage)
