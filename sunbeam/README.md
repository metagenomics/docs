# Sunbeam

[Sunbeam](https://github.com/sunbeam-labs/sunbeam) is a pipeline written in snakemake
that simplifies and automates many of the steps in metagenomic sequencing analysis.

Here is a brief note to help you use sunbeam at Pasteur on TARS. You can also read the
full [documentation](https://sunbeam.readthedocs.io/en/latest/?badge=latest).

## Install

Procedure is very similar to what you would do locally. Install deal with conda install
if you are not using conda. If you want the latest available version:

```bash
git clone https://github.com/eclarke/sunbeam
cd sunbeam
bash install.sh
source activate sunbeam
```

> **Warning** You need internet access to do that so you need to perform install on
the head of submission.

## Run sunbeam

You can follow the [documentation](https://sunbeam.readthedocs.io/en/latest/?badge=latest)
for the initialization of your analysis directory. This will generate a config file in this
directory that we will call `sunbeam_config.yml`.

We also consider that we are working on the dedicated `atm` queue.

Once done you can run sunbeam for the quality control on tars:

```bash
sbatch --qos=atm -p atm -c 1 sunbeam run --configfile sunbeam_config.yml all_qc --jobs 10 --cluster-config cluster.yml --cluster "sbatch --qos=atm -p atm -c {threads}"
```

Refer to the [documentation](https://sunbeam.readthedocs.io/en/latest/?badge=latest) to
see all the different pipelines available.

You can also personalize required resources by adding a `cluster.yml` file as described in
this [page](https://gitlab.pasteur.fr/metagenomics/snakemake/tree/master/workflows#using-on-tars)
