# Simulation

[InSilicoSeq](https://github.com/HadrienG/InSilicoSeq) is a sequencing simulator producing
realistic Illumina reads. Primarily intended for simulating metagenomic samples, it can also
be used to produce sequencing data from a single genome.

We describe here only the usage for a simple simulation. For a more exhaustive documentation,
please refer to [InSilicoSeq documentation](https://insilicoseq.readthedocs.io/en/latest/?badge=latest)
or look at the help section of the tool with `iss generate --help`.

## Install

You have several ways of installing InSilico seq that can be found [Here](https://insilicoseq.readthedocs.io/en/latest/iss/install.html).

Here, we will use of the docker image `hadrieng/insilicoseq:1.4.2` or the singularity one built from the docker image.

## Run de novo metagenomes simulation

### Build your reference metagenome

First step you need to do is select genomes to build your own metagenome. You just need to select genomes of
interest and put them in one `fasta` file.

> **Note** InSilicoSeq can [download and build a metagenome for you](https://github.com/HadrienG/InSilicoSeq#generate-reads-without-input-genomes) but you need internet access.

### With docker

```bash
docker pull hadrieng/insilicoseq:1.4.2
mkdir output
docker run -it -v "/metagenome/folder:/input:rw" -v "/path/to/folder/output/:/output:rw" hadrieng/insilicoseq:1.4.2 iss generate --genomes /input/metagenome.fasta --model HiSeq --output /output/simulation --compress
```

### With singularity on TARS

You can easily build the singularity image from the docker one with `singularity pull NAME.simg docker:hadrieng/insilicoseq:1.4.2` command.


```bash
module load singularity
mkdir output
singularity run /path/to/singularity.simg iss generate --genomes metagenome.fasta --model HiSeq --output /output/simulation --compress
```
